import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/movie/:id',
        name: 'movie',
        component: () =>
            import( /* webpackChunkName: "movie" */ '../views/Movie.vue')
    },
    {
        path: '/search/:query',
        name: 'search',
        component: () =>
            import( /* webpackChunkName: "search" */ '../views/Search.vue')
    },
    {
        path: '/',
        name: 'home',
        component: Home
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router