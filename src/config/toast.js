export default {
    position: "bottom-center",
    duration: 5000,
    keepOnHover: true,
    fitToScreen: true,
    action: [{
        text: 'Close',
        class: 'toast-close',
        onClick: (e, toastObject) => {
            toastObject.goAway(0);
        }
    },
    ]
}