import axios from 'axios';

axios.defaults.validateStatus = function (status) {
    return status < 500; // Resolve only if the status code is less than 500
}

axios.interceptors.response.use(function (response) {

    if (response.status === 401 || response.status === 404) {
        return Promise.reject(response.data.status_message)
    }

    if (response.status >= 400 && response.status < 500) {
        return Promise.reject("An error occurred please contact Support")
    }
    return response;
}, function (error) {
    return Promise.reject(error);
});

export default axios;