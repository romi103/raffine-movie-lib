import Vue from 'vue';
import Vuex from 'vuex';

import api from '../service/api';

Vue.use(Vuex)

const defaultFilters = {
  genre: "",
  year: ""
}

const defaultMovieList = {
  results: [],
  page: 1,
  total_results: 1,
  total_pages: 1,
}

const GUEST_ID = "MovieLibrary.GuestId"

export default new Vuex.Store({
  state: {
    movieList: defaultMovieList,
    filters: defaultFilters,
    searchQuery: null,
    guestSession: {
      id: ""
    }
  },
  mutations: {
    setMovieList(state, movieList) {
      state.movieList = { ...movieList }
    },

    setGenre(state, genre) {
      state.filters.genre = genre
    },

    setYear(state, year) {
      state.filters.year = year
    },

    clearMovieList(state) {
      state.movieList = defaultMovieList
    },

    setGuestId(state, session) {
      state.guestSession = session
    },

    setSearchQuery(state, query) {
      state.searchQuery = query;
    },

    resetSearchQuery(state) {
      state.searchQuery = null;
    }


  },
  actions: {
    async fetchMovie({ commit, state }, page) {
      const currentGenre = state.filters.genre;
      const currentYear = state.filters.year;

      try {
        if (state.searchQuery) {
          commit('setMovieList', await api.searchMovie(page, state.searchQuery))

        } else if (currentGenre || currentYear) {
          commit('setMovieList', await api.fetchByFilters(page, { genre: currentGenre, year: currentYear }))
        } else {
          commit('setMovieList', await api.fetchPopular(page))
        }

      } catch (err) {
        Vue.toasted.error(err);
      }

    },

    async search({ commit, dispatch, state }, query) {


      commit("setSearchQuery", query);
      dispatch("fetchMovie");

    },


    async fetchPopular({ commit }, page) {

      try {
        commit('setMovieList', await api.fetchPopular(page))
      } catch (err) {
        Vue.toasted.error(err);
      }

    },
    async searchMovie({ commit }, query) {
      try {
        commit('setMovieList', await api.searchMovie(query))
      } catch (err) {
        Vue.toasted.error(err);
      }
    },

    async generateGuestId({ commit }) {

      try {
        if (sessionStorage.getItem(GUEST_ID)) {

          const session = JSON.parse(sessionStorage.getItem(GUEST_ID))
          const sessionExpireDate = new Date(session.expires_at);
          const now = new Date();


          if (sessionExpireDate < now) {
            api.generateGuestId().then((res) => {
              sessionStorage.setItem(GUEST_ID, JSON.stringify(res))

              commit("setGuestId", res)


            })
          } else {
            commit("setGuestId", session)
          }
        } else {
          api.generateGuestId().then((res) => {
            sessionStorage.setItem(GUEST_ID, JSON.stringify(res))
            commit("setGuestId", res)
          })
        }
      } catch (err) {
        Vue.toasted.error(err);
      }
    },

  },
  modules: {

  }
})
