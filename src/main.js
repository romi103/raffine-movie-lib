import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import Toasted from 'vue-toasted';
import axios from "./config/axiosConfig";
import vSelect from "vue-select";
import Paginate from 'vuejs-paginate';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faStar } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import toastOptions from "./config/toast";

library.add(faStar)

Vue.component('font-awesome-icon', FontAwesomeIcon)


Vue.config.productionTip = false

Vue.use(Toasted, toastOptions)
Vue.component("v-select", vSelect);
Vue.component('paginate', Paginate)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
