import axios from "axios";

const API_KEY = process.env.VUE_APP_API_KEY;
const path = "https://api.themoviedb.org/3/";

export default {
    async fetchPopular(page = 1) {
        return axios.get(`${path}movie/popular?api_key=${API_KEY}&page=${page}`).then(res => {
            return res.data;
        })
    },

    async fetchByFilters(page = 1, { genre = "", year = "" }) {
        return axios.get(`${path}discover/movie?api_key=${API_KEY}&page=${page}&with_genres=${genre}&year=${year}`).then(res => {
            return res.data;
        })
    },

    async fetchMovieInfo(id) {
        return axios.get(`${path}movie/${id}?api_key=${API_KEY}`).then(res => {
            return res.data
        })
    },

    async fetchMovieCredits(id) {
        return axios.get(`${path}movie/${id}/credits?api_key=${API_KEY}`).then(res => {
            return res.data
        })
    },

    async fetchConfig() {
        return axios.get(`${path}configuration?api_key=${API_KEY}`).then((res) => {
            return res.data;
        })
    },

    async fetchPoster(baseUrl, size, posterPath) {
        return axios.get(`${baseUrl}${size}${posterPath}`).then((res) => {
            return res.data;
        })
    },

    async searchMovie(page = 1, query) {
        return axios.get(`${path}search/movie?query=${query}&api_key=${API_KEY}&page=${page}`).then((res) => {


            return res.data;
        })
    },

    async fetchGenres() {
        return axios.get(`${path}genre/movie/list?api_key=${API_KEY}`).then((res) => {
            return res.data.genres;
        })
    },



    async generateGuestId() {
        return axios.get(`${path}authentication/guest_session/new?api_key=${API_KEY}`).then((res) => {

            return res.data;
        })
    },


    async rateMovie(rating, movieId, sessionId) {
        return axios.post(`${path}movie/${movieId}/rating?api_key=${API_KEY}&guest_session_id=${sessionId}`, { value: rating }).then((res) => {
            return res.data;
        })
    }
}